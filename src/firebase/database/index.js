//CRUD
import firebase from 'firebase';

// Create
export const saveSimulation = (userId, data) => {
    firebase.database()
    .ref('users/' + userId + '/' + data.name)
    .set({
        name: data.name,
        numbersimulation: data.numberSimulation,
        data: {
            datain: data.datain,
            dataout: data.dataout
        }
    });
}
export const verificate = (userId, data) => {
    return firebase.database()
    .ref('users/' + userId + '/' + data)
    .once('value').then(snap => snap.val());

}





// Read
export const getSimulations = () => {
    let userId = JSON.parse(localStorage.getItem('user')).uid;
    return firebase.database()
    .ref('/users/' + userId)
    .once('value')
    .then(snap => snap.val());
}

// Read last
/*
export const onSimulation = (limited) => {
    let userId = JSON.parse(localStorage.getItem('user')).uid;
    return firebase.database()
    .ref('/users/' + userId).limitToLast(limited)
    .once('value')
    .then(snap => snap.val());
}
*/


// Read last
export const onSimulation = (limited) => {
    let userId = JSON.parse(localStorage.getItem('user')).uid;
    return firebase.database()
    .ref('/users/' + userId).orderByChild("numbersimulation").limitToLast(limited)
    .once('value')
    .then(snap => snap.val());
}


// Get next simulation
export const nextSimulation = async() => {
    let userId = await JSON.parse(localStorage.getItem('user')).uid;
    return firebase.database()
    .ref('/users/' + userId)
    .once('value')
    .then(snap => snap.numChildren() + 1);
}

// Update
// No option
export const editSimulation = (userId, data) => {
  firebase.database()
    .ref('users/' + userId + '/' + data.name)
    .update({
        name: data.name,
        numbersimulation: data.numberSimulation,
        data: {
            datain: data.datain,
            dataout: data.dataout
        }
    });
}


// Delet
// No option
export const removeSimulation = (userId, name) => {
  firebase.database()
    .ref('users/' + userId + '/' + name)
    .remove()
}
