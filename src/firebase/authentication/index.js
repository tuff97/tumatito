// Packages
import firebase from 'firebase';


// Create user
export const RegisterUSer = (email, password) => {
    return firebase.auth().createUserWithEmailAndPassword(email, password);
}

// is auth?
export const isAuth = () => {
    //return firebase.auth().currentUser
    if(localStorage.getItem('user')) return true;
    else return false;
}


// Reset password
export const forgotPass = (email) => {
    return firebase.auth()
    .sendPasswordResetEmail(email)
    .then(() => true)
    .catch((err) => {
        console.log('error', err.message);
        return false;
    });
}

// Login
export const addUser = (user) => {
    localStorage.setItem('user', JSON.stringify(user));
}

export const login = (email, password) => {
    return firebase.auth().signInWithEmailAndPassword(email, password)
}



// Logout
const removeUser = () => {
    localStorage.removeItem('user');
}
export const logout = () => {
    removeUser();
    return firebase.auth().signOut();
}
