// Dependencies
import React from 'react';
import { hydrate, render } from 'react-dom';
import firebase from 'firebase';

// firebase
import { firebaseConfig } from './firebase';
// Initialize Firebase
  firebase.initializeApp(firebaseConfig);

// App
import App from './App';



// Element app
const idAPP = document.getElementById('app');

// Render
render(
    <App />,
    idAPP  
);