// Core packages
import { combineReducers } from 'redux';

// Reduc
import home from '../containers/Home/reducer';
import formIn from '../components/Form/reducer';



const AppReducer = combineReducers({
    home,
    formIn
})

export default AppReducer;
