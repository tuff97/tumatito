// Core packages
import { createStore, applyMiddleware } from 'redux';

// Middleware
import thunk from 'redux-thunk';

// Reducers
import AppReducer from '../reducers';


const store = createStore(
  AppReducer,
  applyMiddleware(thunk)
);

export default store;

