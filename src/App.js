// Core packages
import React from 'react';
import { BrowserRouter } from 'react-router-dom';


// Redux
import { Provider } from 'react-redux'

// Store
import store from './store';

// Routes
import Router from './router';

import './assets/styles/main.scss';

const App = () =>
  <Provider store={store}>
    <BrowserRouter>
      <Router />
    </BrowserRouter>
  </Provider>


export default App;