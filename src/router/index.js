// Core packages
import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import Login from '../containers/Login';
import Register from '../containers/Register';
import ForgotPass from '../containers/ForgotPass';
import FormSim from '../containers/FormSim';
import DataSim from '../containers/DataSim';
import Stats from '../containers/Stats';
import History from '../containers/History'
import Home from '../containers/Home';

// auth
import { isAuth } from '../firebase/authentication';


// Layouts
import WithAside from '../containers/Layouts';

const AppRoute = ({ component: Component }) => (
  <Route render={props => (
    console.log('isauth()', isAuth()),
    isAuth()
      ?
      <WithAside {...props}>
        <Component {...props} />
      </WithAside>
      :
      <Redirect to='/ingresar' />
  )} />
)


const DashboardRoute = ({ component: Component }) => (
  <Route render={props => (
    !isAuth()
      ? <Component {...props} />
      : <Redirect to='/datos' />
  )} />
)

const DataRoute = ({ component: Component }) => (
  <Route render={props => (
    isAuth()
      ?
      <WithAside {...props}>
        <Component {...props} />
      </WithAside>
      // <Component {...props}/>
      : <Redirect to='/datos' />
  )} />
)

const Router = () =>
  <Switch>
    <DashboardRoute
      exact
      path='/ingresar'
      component={Login}
    />
    <DashboardRoute
      exact
      path='/registrarse'
      component={Register}
    />
    <DashboardRoute
      exact
      path='/recuperar-cuenta'
      component={ForgotPass}
    />

    <DataRoute
      exact
      path='/datos-entrada'
      component={FormSim}
    />

    <AppRoute
      exact
      path='/datos'
      component={DataSim}
    />
    <AppRoute
      exact
      path='/estadisticas'
      component={Stats}
    />
    <AppRoute
      exact
      path='/historial'
      component={History}
    />
    <AppRoute
      exact
      path='/'
      component={Home}
    />
  </Switch>

export default Router;
