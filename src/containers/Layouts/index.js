// PAckages
import React, { Component } from 'react';

// Components
import Navigation from '../../components/Navigation'


// Layout del Sitio Web ya que no todas las páginas tienen la barra de navegación
class WithAside extends Component {
    render() {
        // Obtenemos todo el contenido de la pagina a renderear
        const body = this.props.children
        return (
            <div>
                <div>
                    <Navigation history={this.props.history}/>
                </div>
                {body}
            </div>
        );
    }
}

export default WithAside;
