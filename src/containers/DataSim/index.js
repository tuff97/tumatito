// Packages
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom'

// firebase
import { onSimulation } from '../../firebase/database';

// Components
import Form from '../../components/Form';
import FormEdit from '../../components/FormEdit';

class DataSim extends Component {
    // Instanciamos el constructor para los valores iniciales
    constructor(props) {
        super(props);
        this.state = {
            data: '',
            isEdit: false,
        }
    }

    // Obtenemos la última simulación
    componentWillMount() {
        let array = {};
        onSimulation(1).then(data => {
            Object.keys(data).forEach(element => {
                console.log('d', data)
                const obj = {
                    datain: data[element].data.datain,
                    dataout: data[element].data.dataout,
                    name: data[element].name,
                    numberSimulation: data[element].numberSimulation
                }
                array = obj;
                console.log('array', array)
            });

            this.setState({
                data: array
            })

        })
    }

    onStateEdit() {
      this.setState({
        isEdit: true,
      })
    }
    render() {
        // Le mandamos por pros la ultima simulación al formuladio indicando que no nesecitamos etiqueta input si no un span
        return (
            <div>
                <div className='btn-left'>
                        <button className='btn' onClick={this.onStateEdit.bind(this)}>Editar simulación</button>
                    </div>
                  <span>{this.state.isEdit}</span>
                  {
                    !this.state.isEdit &&
                    <Form data={this.state.data} tagInput={this.state.isEdit}/>
                  }
                  {
                    this.state.isEdit &&
                    <FormEdit data={this.state.data} tagInput={this.state.isEdit} history={this.props.history}/>
                  }
            </div>
        );
    }
}
const mapStateToProps = ({ formIn }) => {
    return {

        data: formIn.dataIn,
    }

}

{/* <div className='btn-left'>
                        <Link to={{
    pathname: "/",
    state: { editSimulation: true }
  }} className='btn'>Editar simulación</Link>
                    </div> */}

export default connect(mapStateToProps,  {}) (DataSim);
