// Packages
import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';

// firebase
import { login, addUser } from '../../firebase/authentication';

//Notification
import { Notification } from '../../components/Notification';


// Components
import { SVG } from '../../components/SVG';
import { Input } from '../../components/Input';


// styles
import './style.scss';

class Login extends Component {
    // Instanciamos el constructor para los valores iniciales del correo, contraseña y el estado de     la notificación
    constructor() {
        super();
        this.state = {
            email: '',
            pass: '',
            notification: false,
            notifyType: 'failed',
        }
    }

     // Actualiza el estado de los cambos de correo y contraseña
    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value,
        });
    }

    //Click al iniciar sesión
    onClick(e) {
         // Evitamos que la pagina se regargue
        e.preventDefault();

        // Obtenemos el estado del inicio de sesión
        const data = this.state;

        // Función de firebase para validar el inicio de sesión y nos devuelve una promise
        login(data.email, data.pass)
        // Si el inicio de sesión es correcto
        .then((value) => {
            const user = {
                email: value.user.email,
                uid: value.user.uid,
            }
            // Seteamos en el localStorage el usuario para guardaer sesióm
            addUser(user);
            // Lanzamos la notificación de que fue correcto el inicio de sesión
            this.setState({
                notification: true,
                notifyType: 'Success',
            })
            // Esperamos 1 segundo para dirigirlo a el formulario
            setTimeout(() => {
                this.props.history.push('/');
            }, 1000);
        })
        // Si el inicio de sesión es incorrecto
        .catch(err => {
            // Lanzamos la notificación de que fue incorrecto el inicio de sesión
            this.setState({
                notification: true,
                notifyType: 'Failed',
            })
            setTimeout(() => {
                this.setState({
                    notification: false,
                })
            },3000)
            console.log(err)
        });
    }

    render() {
        // Maquetación del formulario de registro que esta hecho en JSX no HTML
        return (
            <div className='login-register'>
                <form className='login-register__form'>
                    <div className='login-register__logo'>
                        <SVG size={60}/>
                        <p>Iniciar sesión en<span>tumatito</span></p>
                    </div>
                    <div className='container_input'>

                        <Input
                            tagInput
                            name={'email'}
                            value={this.state.email}
                            onChange={this.onChange.bind(this)}
                            title={'Correo'}
                            id={'email'}
                            type={'email'}
                            unit={false}
                            required={false}
                            />
                    </div>
                    <div className='container_input'>

                        <Input
                            tagInput
                            name={'pass'}
                            value={this.state.pass}
                            onChange={this.onChange.bind(this)}
                            title={'Contraseña'}
                            id={'password'}
                            type={'password'}
                            unit={false}
                            required={false}
                            />
                    </div>
                    <div className='container_input login-register__links'>
                        <Link to='/recuperar-cuenta'>¿Olvidaste tu contraseña?</Link>
                        <Link to='/registrarse'>Registrarse</Link>
                    </div>
                    <div className='container_input container__btn'>
                        <button className='btn' onClick={this.onClick.bind(this)}>Ingresar</button>
                    </div>
                </form>

                {
                    // Mostramos la notificación dependiendo del estado si es Success o Failed
                    this.state.notification &&
                        <Notification
                        type={this.state.notifyType}
                        title={this.state.notifyType}
                        description={'Inicio de sesión'}/>
                }
            </div>
        );
    }
}


// exportamos El componente de Login para ser utilizado en otro lado
export default Login;
