// Packages
import React, { Component } from 'react';

import { onSimulation } from '../../firebase/database';

// Components
import Form from '../../components/Form';

class FormSim extends Component {


    // función a eliminar-----------
    // componentWillMount() {
    //     onSimulation(2)
    //     .then((value) => {
    //         console.log('valueooo: ', value)
    //     })


    // }

    render() {
        // Instanciamos el formulario para que el usuario pueda ingresar los datos con el atributo "tagInput"
        console.log('props', this.props.location.state)

        return (
            <div>
                <Form history={this.props.history} btn tagInput data={false}/>
            </div>
        );
    }
}

export default FormSim;
