// Packages
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// auth
import { forgotPass } from '../../firebase/authentication';

// Notification
import { Notification } from '../../components/Notification';

// Components
import { SVG } from '../../components/SVG';
import { Input } from '../../components/Input';

class ForgotPass extends Component {

    // Instanciamos el constructor para los valores iniciales del correo, contraseña y el estado de     la notificación
    constructor() {
        super();
        this.state = {
            email: '',
            notification: false,

        }
    }

     // Actualiza el estado de los cambos de correo y contraseña
    onChange(e) {
        console.log(e.target.value)
        this.setState({
            [e.target.name]: e.target.value,
        });
    }

    // Click al mandar el correo electronico para establecer la nueva contarseña
    onClick(e) {
        e.preventDefault();
        this.setState({
            notification: true,
        })
        // Esperamos 1 segundo y lo dirigimos a el inicio de sesión
        setTimeout(() => {
            this.props.history.push('/ingresar');
        },1000)
        forgotPass(this.state.email);
    }

    render() {
        // Maquetación del formulario de registro que esta hecho en JSX no HTML
        return (
            <div className='login-register'>
                <form className='login-register__form'>
                    <div className='login-register__logo'>
                        <SVG size={60}/>
                        <p>Reestablece tu contraseña de<span>tumatito</span></p>
                    </div>
                    <div className='container_input'>

                        <Input
                            tagInput
                            name={'email'}
                            value={this.state.email}
                            onChange={this.onChange.bind(this)}
                            title={'Correo'}
                            id={'email'}
                            type={'email'}
                            unit={false}
                            required={false}
                            />
                    </div>
                    <div className='container_input login-register__links'>
                        <Link to='/ingresar'>Regresar</Link>
                    </div>
                    <div className='container_input container__btn'>
                        <button className='btn' onClick={this.onClick.bind(this)}>Enviar</button>
                    </div>
                </form>
                {
                    // Maquetación del formulario de registro que esta hecho en JSX no HTML
                    this.state.notification &&
                        <Notification
                        type={'Success'}
                        title={'Success'}
                        description={'Correo enviado'}/>
                }
            </div>
        );
    }
}

// exportamos El componente de ForgotPass para ser utilizado en otro lado
export default ForgotPass;
