// Packages
import React, { Component } from 'react';

// firebase
import { getSimulations } from '../../firebase/database';

// Components
import Table from '../../components/Table';

class History extends Component {

    // Instanciamos el constructor para inicar el arreglo de objetos en vacio
    constructor(props) {
        super(props);
        this.state = {
            data: [],
        }
    }
    componentDidMount() {
        // Creamos un arreglo para guardar todas la simulaciones del usuario
        let array = [];
        // Obtenemos y recorremos todas las simulaciones del usuario
        getSimulations().then(data => {
            Object.keys(data).forEach(element => {
                const obj = {
                    datain: data[element].data.datain,
                    dataout: data[element].data.dataout,
                    name: data[element].name,
                    numberSimulation: data[element].numberSimulation
                }

                // Guardamos cada simulación en el arreglo declarado con anterioridad
                array.push(obj)
            });

            // Guardamos el arreglo dentro de data del estado del componente
            this.setState({
                data: array
            })
        })
    }

    onRefesh() {
       // Creamos un arreglo para guardar todas la simulaciones del usuario
       let array = [];
       // Obtenemos y recorremos todas las simulaciones del usuario
       getSimulations().then(data => {
           Object.keys(data).forEach(element => {
               const obj = {
                   datain: data[element].data.datain,
                   dataout: data[element].data.dataout,
                   name: data[element].name,
                   numberSimulation: data[element].numberSimulation
               }

               // Guardamos cada simulación en el arreglo declarado con anterioridad
               array.push(obj)
           });

           // Guardamos el arreglo dentro de data del estado del componente
           this.setState({
               data: array
           })
       })
    }
    render() {

        // Le enviamos las simulaciones al componente Table
        return (
            <div>
                <Table data={this.state.data} onRefesh={this.onRefesh.bind(this)}/>
            </div>
        );
    }
}

export default History;
