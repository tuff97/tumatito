// Packages
import React, { Component } from 'react';
import { connect } from 'react-redux';

// Database
import { saveSimulation } from '../../firebase/database';

// Notification
import { Notification } from '../../components/Notification';
// Actions
import { stop, stopPlant, setPlant } from './actions';
import { setDataIn, clearData } from '../../components/Form/actions';

// SVG´s
import { PlantHome } from '../../components/SVG';

// style
import './style.scss';




// Componente presentacional para mostrar las plantas
const Plant = props => {
    return (
        <div className='container__plant'>
            <PlantHome plant={props.plant} />
        </div>
    );
}


class Home extends Component {

    // Instancicamos el constructor para los valores iniciales
    constructor() {
        super();
        this.state = {
            plant: 'semana1',
            setInterval: null,

            waterPlant: 56,
            notification: false,
        }
    }

    async componentWillMount() {

        // Validamos si la simulación es nueva
        if(this.props.data.name !== '' && this.props.finalPlant) {
            console.log('is new')
            // Obtenemos el estado de la planta
            const data = this.props.data;

            // Iniciamos la animación
            this.animationPlant();

            // Hacemos el calculo  de las plantas que tendrá el invernadero
            this.CalculatePlants(data.datain.greenhouse.width, data.datain.greenhouse.height);

            // Validamos la cantidad de agua
            this.valueWater();

            // Validamos los nutrientes
            this.ValueNutrientes();

            // Validamos la temperatura
            this.valueTemperature(
                data.datain.greenhouse.daytemperature,
                data.datain.greenhouse.nighttemperature,
                data.dataout.totalharvest
            );

            // Guardamos los datos uintroducidos
            this.props.setDataIn(data);

            // Guardamos los resultados calculados en la base de datos
            this.saveDatabase();

            // Detenemos la animación de las plantas
            await this.props.stopPlant(true);
        }else {
            // La simulación ya esxiste y ya no volvemos a hacer la animación del crecimiento de la splantas
            await this.props.setPlant(9);

        }

    }

    // funcion que se ejecuta al desmontar el componente
    componentWillUnmount() {
        // limpiamos el "Intervalo"
        clearInterval(this.state.setInterval)
    }

    // Guardado de los resultados en la vase de datos
    saveDatabase() {
        const data = this.props.data;
        const uid = JSON.parse(localStorage.getItem('user')).uid;
        saveSimulation(uid, data);
        this.props.clearData();
    }
    // Animación de las plantas
    async animationPlant() {
        await this.props.setPlant(1);
        let account = 1;
        let interval = null;
        if(this.props.finalPlant) {
            interval = setInterval(() => {
                if(account < 10) {
                    this.props.setPlant(account);
                    account++;
                }else {
                    this.setState({
                        notification: true
                    })
                    setTimeout(() => {
                        this.setState({
                            notification: false
                        })
                    }, 2000)
                    this.props.stopPlant(true);
                }
            }, 1000);
        }
        this.setState({
            setInterval: interval
        })
    }
    // random "Obtenemos un numero random de un 'Intervalo'"
    random(min, max) {
        return parseInt(Math.random() * (max - min) + min);
    }

    /*-------------------  * Calculate  -------------------*/
    // Función para calcular las Plantas
    CalculatePlants(width, height) {
        let ancho = width / 2;
        let largo = ((height * 100) / 40)/2;
        const totalPlants = parseInt(ancho * largo);
        const dataOut = this.props.data.dataout;
        dataOut.totalplants = totalPlants;
        //this.props.setTotalPlants(totalPlants);
    }

    // Función para el calculo del Agua
    valueWater() {
        const data = this.props.data;
        let jitomatePorPlanta = 0;
        if(data.datain.waterproperties.quantity) {
            // Si introdujo agua

            // Agua qu enesecita
            const water = (data.dataout.totalplants  * this.state.waterPlant);
            data.dataout.consumerwater = water;


            if(data.datain.waterproperties.quantity >= water) {
                // Si le alcanza y se reparte
                for(let i = 0; i < data.dataout.totalplants; i++) {
                    jitomatePorPlanta = jitomatePorPlanta + this.random(7, 9);
                }


            } else {

                let aguaPorPlantas = data.datain.waterproperties.quantity / data.dataout.totalplants;

                // let aguaPorPlantas = this.state.waterQuantity / this.state.plantsQuantity;
                //No le alcanzo
                //caso 1 nivel muy bajo la planta no crece 35L
                if(aguaPorPlantas <= 25) {
                    // se mueres
                    /*this.setState({
                        stateWater: 'Murio'
                    })*/

                } else if(aguaPorPlantas > 25 && aguaPorPlantas <= 35) {
                    /*this.setState({
                        stateWater: 'Bajo'
                    })*/
                    // 1-3 jitomates
                    for(let i = 0; i < data.dataout.totalplants; i++) {
                        jitomatePorPlanta = jitomatePorPlanta + this.random(1, 3);
                    }


                } else if(aguaPorPlantas > 35 && aguaPorPlantas < 45) {
                    /*this.setState({
                        stateWater: 'Medio'
                    })*/
                    // 4-6 jitomates
                    for(let i = 0; i < data.dataout.totalplants; i++) {
                        jitomatePorPlanta = jitomatePorPlanta + this.random(4, 6);
                    }


                } else if(aguaPorPlantas >= 45) {
                    /*this.setState({
                        stateWater: 'Alto'
                    })*/
                    // 6-7 jitomates
                    for(let i = 0; i < data.dataout.totalplants; i++) {
                        jitomatePorPlanta = jitomatePorPlanta + this.random(6, 7);
                    }
                }
            }

        } else {
            // redux
            const defaultWater = data.dataout.totalplants * 56;
            data.dataout.consumerwater = defaultWater;

            for(let i = 0; i < data.dataout.totalplants; i++) {
                jitomatePorPlanta = jitomatePorPlanta + this.random(7, 9);
            }
        }
        const promedioPlanta = jitomatePorPlanta / data.dataout.totalplants;
        data.dataout.totalharvest = (jitomatePorPlanta >=0 )? jitomatePorPlanta: 0;
        data.dataout.tomatoaverage = (parseInt(promedioPlanta) >= 0)? parseInt(promedioPlanta):0;


        /*this.setState({
            jitomates: jitomatePorPlanta,
        });*/
    }
    // Función para el calculo de los nutrientes
    ValueNutrientes() {
        const data = this.props.data;
        let jitomates = data.dataout.totalharvest;

        const nutrientsFosforo = data.datain.nutrients.fosforo;
        const nutrientsNitrogeno = data.datain.nutrients.nitrogeno;
        const nutrientsPotasio = data.datain.nutrients.potasio;

        if(nutrientsFosforo >= 7.5) {
            data.dataout.cnfosforo = 7.5;
        } else {
            data.dataout.cnfosforo = 0 ;
        }

        if(nutrientsNitrogeno >= 60) {
            data.dataout.cnnitrogeno = 60;
        } else {
            data.dataout.cnnitrogeno = 0;
        }

        if(nutrientsPotasio >= 50) {
            data.dataout.cnpotasio = 50;
        } else {
            data.dataout.cnpotasio = 0;
        }

        // bajo
        if((nutrientsFosforo >= 2 && nutrientsFosforo <= 2.4) ||
            (nutrientsNitrogeno >= 25 && nutrientsNitrogeno <= 39.9) ||
            (nutrientsPotasio >= 10 && nutrientsPotasio <= 28)
            ) {
            for(let i = 0; i < data.dataout.totalplants; i++) {
                jitomates = jitomates - this.random(1, 3);
            }
        // muere
        } else if((nutrientsFosforo < 2.5 ) &&
            (nutrientsNitrogeno < 40 ) &&
            (nutrientsPotasio < 29)
            ) {
            /*this.setState({
                jitomates: 0,
            })*/
        // optimo
        } else if((nutrientsFosforo >= 2.5 && nutrientsFosforo <= 7.5) ||
            (nutrientsNitrogeno >= 40 && nutrientsNitrogeno <= 60) ||
            (nutrientsPotasio >= 29 && nutrientsPotasio <= 50)
            ) {
                //posible a dar mas
                for(let i = 0; i < data.dataout.totalplants; i++) {
                    jitomates = jitomates + this.random(1, 2);
                }
            // for(let i = 0; i < this.state.plantsQuantity; i++) {
            //   jitomates = jitomates - this.getRandomArbitrary(1, 2);
            // }
        // desperdicio
        } else if(nutrientsFosforo >= 7.5
            && nutrientsNitrogeno > 60
            && nutrientsPotasio >= 50
            ) {
        }
        data.dataout.totalharvest = (jitomates > 0) ? jitomates :0;
        /*this.setState({
            jitomates: jitomates,
        })*/

    }

    // Función para el calculo de la Temperatura
    valueTemperature(day, night, totalharvest) {
        const data = this.props.data;
        if(!(day >= 21 && day <= 30) || !(night >= 18 && night <= 21)) {
            data.dataout.totalharvest = (totalharvest - (80 * this.random(1, 7)) >= 0) ? (totalharvest - (80 * this.random(1, 7))): 0;
        }
    }


    render() {
        // Contruimps la ruta de la imagen de cada planta
        const plant = 'semana'+this.props.plant;

        // Maquetación del formulario de registro que esta hecho en JSX no HTML
        return (
            <div className='home'>
                <header className='stats__header'>
                    <h1 className='home__title'>Simulación</h1>
                </header>
                <div className='home__content'>
                    <Plant plant={plant}/>
                    <Plant plant={plant}/>
                    <Plant plant={plant}/>
                    <Plant plant={plant}/>
                    <Plant plant={plant}/>
                    <Plant plant={plant}/>
                    <Plant plant={plant}/>
                    <Plant plant={plant}/>
                    <Plant plant={plant}/>
                    <Plant plant={plant}/>
                    <Plant plant={plant}/>
                    <Plant plant={plant}/>
                    <Plant plant={plant}/>
                    <Plant plant={plant}/>
                    <Plant plant={plant}/>
                </div>
                {
                    // Mostramos la notificación dependiendo del estado si es Success o Failed
                    this.state.notification &&
                        <Notification
                        type={'Success'}
                        title={'Simulación terminada'}
                        description={'La simulación fue concluida con exito'}/>
                }
            </div>
        );
    }
}

// Para obtener los datos guardados en el store de Redux
const mapStateToProps = ({ home, formIn }) => {
    return {
        loading: home.loading,
        finalPlant: home.finalPlant,
        plant: home.plant,

        data: formIn.dataIn,
    }
}

// Conectar componente con Redux
export default connect(mapStateToProps, { stop, stopPlant, setPlant, setDataIn, clearData }) (Home);
