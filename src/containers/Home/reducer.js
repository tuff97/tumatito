const initialState = {
    loading: true,
    finalPlant: true,
    plant: 9,
}




export default (state = initialState, action) => {
    switch (action.type) {
        case 'STOP_LOADING':
            return Object.assign({}, state, {
                loading: false,
            });
        case 'STOP_PLANT':
            return Object.assign({}, state, {
                finalPlant: action.data  
            });
        case 'RESET_PLANT':
                return Object.assign({}, state, {
                    finalPlant: true,    
                });
        case 'SET_PLANT':
            return Object.assign({}, state, {
                plant: action.data,   
            });

    
        default:
            return state;
    }
}