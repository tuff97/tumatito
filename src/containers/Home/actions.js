export const stop = () => {
    return dispatch => {
        dispatch({
            type: 'STOP_LOADING',
        })
    }
}

export const stopPlant = (data) => {
    return dispatch => {
        dispatch({
            type: 'STOP_PLANT',
            data: data,
        })
    }
}
export const resetPlant = () => {
    return dispatch => {
        dispatch({
            type: 'RESET_PLANT',
        })
    }
}


export const setPlant = (plant) => {
    return dispatch => {
        dispatch({
            type: 'SET_PLANT',
            data: plant
        })
    }
}