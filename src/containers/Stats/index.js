// Packages
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// firebase
import { onSimulation } from '../../firebase/database';

// Components
import Graphics from '../../components/Graphics';
import { PlantSVG } from '../../components/SVG';

// Styles
import './style.scss';


// Componente presentacional para el diseño de cada bloque de información con su titulo
const InfoBlock = (props) => {
    return (
        <article className='info-block'>
            <h3 className='info-block__title'>{props.title}</h3>
            {props.children}
        </article>
    );
}


// Componente presentacional para las barras de progreso
const ProgressBar = (props) => {
    return (
        <div className='stats-progress'>
            <p className='stats-progress__nutrient'>{props.nutrient}</p>
            <div className='stats-progress__container'>
                <p className='stats-progress__data'>{props.consumer}.g<strong>/</strong>{props.initial}g.</p>
                <div className='stats-progress__placeholder'>
                    <div className='stats__progress-bar' style={{width: 180 - (props.consumer * 1.8) + 'px'}}></div>
                </div>
            </div>
        </div>
    );
}


// Componente presentacional en circulo
const Circle = (props) => {
    return(
        <div className={`circle-data circle-data__${props.color}`}>
            {props.data}
        </div>
    );
}

// Componente presentacional para la información del consumo del agua o la producción del jitomate
const InfoCircle = (props) => {
    return(
        <div className='info-circle'>
            <div className='info-circle__left'>
                <Circle data={props.dataleft} color={props.color}/>
                <span className='info-circle__name'>{props.tagleft}</span>
            </div>

            <div className='info-circle__right'>
                <Circle data={props.dataright} color={props.color}/>
                <span className='info-circle__name'>{props.tagright}</span>
            </div>
        </div>
    );
}


// Componente presentacional para el Loading
const Loading = () => {
    return(
        <div className='loading'>
            <p className='loading__msg'>Cargando datos, por favor espere</p>
        </div>
    );
}


class Stats extends Component {
    // Instancicamos el constructor para los valores iniciales del correo, contraseña y el estado de     la notificación
    constructor(props) {
        super(props);
        this.state = {
            data: '',
        }
    }

    // Funcion que se ejecuta antes de montar el componente
    componentDidMount() {

        // creamos variable para guardar los datos de la simulación
        let array = {};

        // Función para guardar los datos de la ultima simulación
        onSimulation(1).then(data => {
          if(data == null) {
            this.setState({
              data: 'empty'
          })
            return
          }
            Object.keys(data).forEach(element => {
                const obj = {
                    datain: data[element].data.datain,
                    dataout: data[element].data.dataout,
                    name: data[element].name,
                    numberSimulation: data[element].numberSimulation
                }
                array = obj
            });
            // Guardamos los datos de la ultima simulación en el estado del componente
            this.setState({
                data: array
            })
        })
    }
    render() {

        // Creamos unas constantes para los datos de entrada y los de salida de la simulación
        const dataout = this.state.data.dataout;
        const datain = this.state.data.datain;

        // Si aún no hay datos que cargue el componente Loading
        if(!this.state.data) {
            return (
                <Loading />
            )
        }

        if(this.state.data === 'empty') {
          return (
              <div className='simulationEmpty'>
                <p>No hay simulaciones</p>
                <Link to='/datos-entrada' className='btn btn-stats__new'>Nueva simulación</Link>
              </div>
          )
      }

        // Maquetación del formulario de registro que esta hecho en JSX no HTML
        return (

            <div className='stats'>
                <header className='stats__header'>
                    <h1 className='stats__title'>Estadísticas</h1>
                    <div className='stats__header__links'>
                        <Link to='/datos-entrada' className='btn btn-stats__new'>Nueva simulación</Link>
                        <Link to='/historial' className='btn btn-stats__history'>Historial</Link>
                    </div>
                </header>
                <Graphics data={this.state.data}/>
                <div className='stats__content'>
                    <InfoBlock title={'Nutrientes que se desperdician'}>
                        <ProgressBar nutrient={'Potasio'} consumer={dataout.cnpotasio} initial={datain.nutrients.potasio}/>
                        <ProgressBar nutrient={'Fósforo'} consumer={dataout.cnfosforo} initial={datain.nutrients.fosforo}/>
                        <ProgressBar nutrient={'Nitrogeno'} consumer={dataout.cnnitrogeno} initial={datain.nutrients.nitrogeno}/>
                    </InfoBlock>

                    <InfoBlock title={'Agua'}>
                        <InfoCircle
                        tagleft={'Total'}
                        dataleft={dataout.userwater}
                        tagright={'Consumida'}
                        dataright={dataout.consumerwater}
                        color={'menta'}/>
                    </InfoBlock>

                    <InfoBlock title={'Jitomate'}>
                        <InfoCircle
                        tagleft={'Total'}
                        dataleft={dataout.totalharvest}
                        tagright={'Promedio'}
                        dataright={dataout.tomatoaverage}
                        color={'tomato'}/>
                    </InfoBlock>

                    <InfoBlock title={'Plantas'}>
                        <div className='info-circle__right'>
                            <PlantSVG />
                            <span className='info-circle__name'>Total <strong>{dataout.totalplants}</strong></span>
                        </div>
                    </InfoBlock>
              </div>

            </div>
        );
    }
}

export default Stats;
