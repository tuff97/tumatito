// Packages
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// firebase
import { RegisterUSer, addUser } from '../../firebase/authentication';

// Notification
import { Notification } from '../../components/Notification';

// Components
import { SVG } from '../../components/SVG';
import { Input } from '../../components/Input';


class Register extends Component {
    // Instanciamos el constructor para los valores iniciales del correo, contraseña y el estado de     la notificación
    constructor() {
        super();
        this.state = {
            email: '',
            pass: '',
            repeatPass: '',
            messageError: false,
            notification: false,

        }
    }

     // Actualiza el estado de los cambos de correo y contraseña
    onChange(e) {
        console.log(e.target.value)
        this.setState({
            [e.target.name]: e.target.value,
            messageError: false,
        });
    }

    //Click al registrarse
    onClick(e) {
         // Evitamos que la pagina se regargue
        e.preventDefault();

        // Validamos que la contraseña debe contener mas de 6 caracteres
        if(this.state.pass.length < 6) {
            this.setState({
                messageError: 'La contraseña debe tener mas de 6 caracteres'
            })
            return;
        }

        // Obtenemos todo el estado de nuestro registro
        const data = this.state;

        //Validamos que todos los campos no esten vacios
        if(data.email === '' || data.pass === '' || data.repeatPass === '') {
            this.setState({
                messageError: 'Por favor introduzca su correo y contraseña'
            })
            return;
        }

        // Validamos que la contraseña sea la misma y el correo no este vacio
        if(data.pass === data.repeatPass && data.email !== '') {
            // Función de firebase que registra al usuario y nos devuelve una promise
            RegisterUSer(data.email, data.pass)
            // Si el registro fue correcto
            .then((value) => {
                
                const user = {
                    email: value.user.email,
                    uid: value.user.uid,
                }
                console.log(user)
            // Seteamos en el localStorage el usuario para guardaer sesióm
                addUser(user);
            // Lanzamos la notificación de que fue correcto el inicio de sesión

                this.setState({
                    messageError: false,

                    notification: true,
                })
                // esperamos 1 segundo para dirigirlo a el formulario
                setTimeout(() => {
                    this.props.history.push('/datos-entrada');                    
                },1000)
            })
            // Si el registro fue incorrecto
            .catch(() => {
                
            })
        }else {
            this.setState({
                messageError: 'Las contraseñas NO coinciden.',
            })
        }
        
    }

    render() {
        // Maquetación del formulario de registro que esta hecho en JSX no HTML
        return (
            <div className='login-register'>
                <form className='login-register__form'>
                    <div className='login-register__logo'>
                        <SVG size={60}/>
                        <p>Registrate en<span>tumatito</span></p>
                    </div>
                    <div className='container_input'>

                        <Input
                            tagInput
                            name={'email'}
                            value={this.state.email}
                            onChange={this.onChange.bind(this)}
                            title={'Correo'}
                            id={'email'}
                            type={'email'}
                            unit={false}
                            required={false}
                            />
                    </div>
                    <div className='container_input'>
                        <Input
                            tagInput
                            name={'pass'}
                            value={this.state.pass}
                            onChange={this.onChange.bind(this)}
                            title={'Contraseña'}
                            id={'password'}
                            type={'password'}
                            unit={false}
                            required={false}
                            />
                    </div>
                    <div className='container_input'>
                        <Input
                            tagInput
                            name={'repeatPass'}
                            value={this.state.repeatPass}
                            onChange={this.onChange.bind(this)}
                            title={'Repetir contraseña'}
                            id={'repeatPass'}
                            type={'password'}
                            unit={false}
                            required={false}
                            />
                    </div>
                    {
                        this.state.messageError &&
                            <p className='error-msg'>{this.state.messageError}</p>
                    }
                    <div className='container_input login-register__links'>
                        <Link to='/ingresar'>Regresar</Link>
                    </div>
                    <div className='container_input container__btn'>
                        <button className='btn' onClick={this.onClick.bind(this)}>Registrarse</button>
                    </div>
                </form>
                {
                    this.state.notification &&
                        <Notification
                        type={'Success'}
                        title={'Success'}
                        description={'Registro Validado'}/>
                }
            </div>
        );
    }
}

// exportamos El componente de Registro para ser utilizado en otro lado
export default Register;
