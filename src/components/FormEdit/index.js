// Packages
import React, { Component } from 'react';
import { connect } from 'react-redux';


// auth
import { logout } from '../../firebase/authentication';
// database
import { editSimulation, nextSimulation } from '../../firebase/database';


// Notification
import { Notification } from '../../components/Notification';

//actions
import { setPlant, resetPlant } from '../../containers/Home/actions';

import { setDataIn } from '../Form/actions';

// Components
import { Input } from '../Input';

// Styles
// import './style.scss';

// Bloque de formulario que lleva el título de cada apartado
const Header = (props) => {
  return (
    <div className='form__header'>
      <div>
        <h2 className='form__header__title'>{props.title}</h2>
      </div>
      <div className='form__header__content'>
        {props.children}
      </div>
    </div>
  );
}

class FormEdit extends Component {
  // Instanciamos el constructor para los valores inicialeas del formulario
  constructor() {
    super();
    this.state = {
      notify: false,
      notifyType: 'Failed',
      nextSimulation: '',
      // GreenHouse
      name: '',
      width: '',
      height: '',

      // Temperature
      Daytemperature: '',
      Nighttemperature: '',

      // Ground propterties
      ground: '',

      // Water properties
      phWater: '',
      conductivity: '',
      quantityWater: '',

      // Nutrients
      fosforo: '',
      nitrogeno: '',
      potasio: '',
      nameUniq: false,
    }
  }

  componentWillMount() {
    // Obtenemos la ultima simlación
    this.nextTest();
    const data = this.props.data.datain
    console.log('simulation ->', this.props.data)
    this.setState({
      notify: false,
      notifyType: 'Failed',
      // nextSimulation: '',
      // GreenHouse
      name: this.props.data.name,
      width: data.greenhouse.width,
      height: data.greenhouse.height,

      // Temperature
      Daytemperature: data.greenhouse.daytemperature,
      Nighttemperature: data.greenhouse.nighttemperature,

      // Ground propterties
      ground: data.groundproperties.ph,

      // Water properties
      phWater: data.waterproperties.ph,
      conductivity: data.waterproperties.conductivity,
      quantityWater: data.waterproperties.quantity,

      // Nutrients
      fosforo: data.nutrients.fosforo,
      nitrogeno: data.nutrients.nitrogeno,
      potasio: data.nutrients.potasio,
      nameUniq: false,
    })
  }

  // Funcion para obtener el numero de la ultima simulación
  nextTest() {
    nextSimulation().then(value => {
      this.setState({
        nextSimulation: value - 1
      })
    })
  }

  // Actualiza el estado de todos los campos del formulario
  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    })
  }


  // onClick() {
  //   logout().then(() => {
  //     // Seteamos la planta en 1 para que la simulación se reinicie
  //     this.props.setPlant(1);
  //     this.props.resetPlant();
  //     // Diriginos al usuario a la pantalla de inicio de sesión
  //     this.props.history.push('/ingresar');
  //   })
  // }

  /// Enviamos los datos del formulario
  onClickSimulation(e) {
    // Evitamos que la pantalla se recargue
    e.preventDefault();
    // Obtenemos todo el estado de los campos del formulario
    const state = this.state;

    // Creamos la estructura deñ registro y lo llenamos con los datos del formulario
    const json = {
      "name": state.name,
      "numberSimulation": state.nextSimulation,
      "datain": {
        "nutrients": {
          "fosforo": state.fosforo,
          "nitrogeno": state.nitrogeno,
          "potasio": state.potasio
        },
        "greenhouse": {
          "width": state.width,
          "height": state.height,
          "daytemperature": state.Daytemperature,
          "nighttemperature": state.Nighttemperature

        },
        "waterproperties": {
          "quantity": state.quantityWater,
          "ph": state.phWater,
          "conductivity": state.conductivity
        },
        "groundproperties": {
          "ph": state.ground
        }
      },
      "dataout": {
        "totalplants": '',
        "totalharvest": '',
        "tomatoaverage": '',
        "consumerwater": '',
        "userwater": state.quantityWater,
        "cnfosforo": '',
        "cnnitrogeno": '',
        "cnpotasio": ''
      }
    }


    // Guardamos el registro
    this.props.setDataIn(json);
    // Validamos que ningun campo quede vacio
    if (
      this.state.name !== '' &&
      this.state.width !== '' &&
      this.state.height !== '' &&
      this.state.nighttemperature !== '' &&
      this.state.daytemperature !== '' &&

      this.state.fosforo !== '' &&
      this.state.nitrogeno !== '' &&
      this.state.potasio !== '' &&

      this.state.quantity !== '' &&
      this.state.conductivity !== '' &&
      this.state.phWater !== '' &&
      this.state.ground !== ''

    ) {

      // Obtenemod el ID del usuario
      const uid = JSON.parse(localStorage.getItem('user')).uid;
      editSimulation(uid, json)
      console.log('this.props', this.props)
      this.props.history.push('/');
      this.setState({
        notify: true,
      })


      // Obtenemod el ID del usuario
      // const uid = JSON.parse(localStorage.getItem('user')).uid;
      // Verificamos que ell nombnte de la simulación no exista
      // verificate(uid, this.state.name).then((value) => {
      //   console.log('exist?', value);
      //   if (value === null) {
      //     this.props.history.push('/');
      //     this.setState({
      //       notify: true,
      //     })
      //   } else {
      //     // Si la simulación esxiste, el usuario deberá cambiarlo
      //     document.getElementById('name').focus();
      //     this.setState({
      //       nameUniq: true,
      //     })
      //   }
      // })


    } else {
      // Si algún campo queda vacio, se le notifica al usuario que tiene que llenar todos los campos
      this.setState({
        notify: true,
      })
      setTimeout(() => {

        this.setState({
          notify: false,
        })
      }, 3000)
    }

  }


  render() {

    const tag = this.props.tagInput;
    let data = this.props.data;
    const name = this.props.data.name;
    data = data.datain;
    return (
      <div>

        <form className='data-form'>
          <Header title={'Invernadero'}>
            <Input
              text={name}
              tagInput={false}
              title={'Nombre de la simulación'}
              type={'text'}
              name={'name'}
              value={this.state.name}
              onChange={this.onChange.bind(this)}
              id={'name'}
              unit={false}
              required={true}
            />

            <Input
              text={data.greenhouse.width}
              tagInput={tag}
              title={'Ancho del invernadero'}
              type={'number'}
              name={'width'}
              value={this.state.width}
              onChange={this.onChange.bind(this)}
              id={'width'}
              unit={'m'}
              required
            />

            <Input
              text={data.greenhouse.height}
              tagInput={tag}
              title={'Largo del invernadero'}
              type={'number'}
              name={'height'}
              value={this.state.height}
              onChange={this.onChange.bind(this)}
              id={'height'}
              unit={'m'}
              required
            />
          </Header>

          <Header title={'Temperatura'}>
            <Input
              text={data.greenhouse.daytemperature}
              tagInput={tag}
              title={'Temperatura del día'}
              type={'number'}
              name={'Daytemperature'}
              value={this.state.Daytemperature}
              onChange={this.onChange.bind(this)}
              id={'Daytemperature'}
              unit={'°C'}
              required
            />

            <Input
              text={data.greenhouse.nighttemperature}
              tagInput={tag}
              title={'Temperatura de la noche'}
              type={'number'}
              name={'Nighttemperature'}
              value={this.state.Nighttemperature}
              onChange={this.onChange.bind(this)}
              id={'Nighttemperature'}
              unit={'°C'}
              required
            />
          </Header>

          <Header title={'Propiedades de la tierra'}>
            <Input
              text={data.groundproperties.ph}
              tagInput={tag}
              title={'Ph'}
              type={'number'}
              name={'ground'}
              value={this.state.ground}
              onChange={this.onChange.bind(this)}
              id={'ground'}
              unit={'Ph'}
              required
            />
          </Header>

          <Header title={'Propiedades del agua'}>
            <Input
              text={data.waterproperties.ph}
              tagInput={tag}
              title={'Ph'}
              type={'number'}
              name={'phWater'}
              value={this.state.phWater}
              onChange={this.onChange.bind(this)}
              id={'phWater'}
              unit={'Ph'}
              required
            />
            {
              tag &&
              <div>
                <span className='question-water'>¿No conoces el dato exacto?</span>
                <p className='paragraph-water'>La cantidad total de agua es un requerimiento base, sin embargo, si usted desconoce el dato exacto puede dejar el campo sin llenar, debido a que el simulador asignará la cantidad optima para la siembra.</p>
              </div>
            }

            <Input
              text={data.waterproperties.conductivity}
              tagInput={tag}
              title={'Conductividad'}
              type={'number'}
              name={'conductivity'}
              value={this.state.conductivity}
              onChange={this.onChange.bind(this)}
              id={'conductivity'}
              unit={'σ'}
              required
            />

            <Input
              text={data.waterproperties.quantity}
              tagInput={tag}
              title={'Cantidad de agua'}
              type={'number'}
              name={'quantityWater'}
              value={this.state.quantityWater}
              onChange={this.onChange.bind(this)}
              id={'quantityWater'}
              unit={'L'}
              required={false}
            />
          </Header>

          <Header title={'Nutrientes'}>
            <Input
              text={data.nutrients.fosforo}
              tagInput={tag}
              title={'Fósforo'}
              type={'number'}
              name={'fosforo'}
              value={this.state.fosforo}
              onChange={this.onChange.bind(this)}
              id={'fosforo'}
              unit={'P'}
              required
            />

            <Input
              text={data.nutrients.nitrogeno}
              tagInput={tag}
              title={'Nitrógeno'}
              type={'number'}
              name={'nitrogeno'}
              value={this.state.nitrogeno}
              onChange={this.onChange.bind(this)}
              id={'nitrogeno'}
              unit={'N'}
              required
            />

            <Input
              text={data.nutrients.potasio}
              tagInput={tag}
              title={'Potasio'}
              type={'number'}
              name={'potasio'}
              value={this.state.potasio}
              onChange={this.onChange.bind(this)}
              id={'potasio'}
              unit={'K'}
              required
            />

          </Header>

          {
            this.state.nameUniq &&
            <p>El nombre de la simulación ya existe, favor de cambiarlo</p>
          }


          <div className='container_input contain-btn__init-simulation'>
            <button className='btn' onClick={this.onClickSimulation.bind(this)}>Iniciar simulación</button>
          </div>

        </form>

        {
          this.state.notify &&
          <Notification
            type={'Filed'}
            title={'Failed'}
            description={'Favor de rellenar todos los campos'} />
        }
      </div>
    );

  }
}

export default connect(null, { setPlant, resetPlant, setDataIn })(FormEdit);
