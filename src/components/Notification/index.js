// Dependencies
import React from 'react';
// Styles
import './style.scss';

// Componente presemtacional de la notificaciones
export const Notification = props => {
    return (
        <div className={`notification notification--${props.type}`}>
            <h4 className={`notification__title--${props.type}`}>{props.title}</h4>
            <p className='notification__description'>{props.description}</p>
        </div>
    );
}