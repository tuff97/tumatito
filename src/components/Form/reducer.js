const initialState = {
    dataIn: {
        "name": '',
        "numberSimulation": '',
        "datain": {
            "nutrients": {
                "fosforo": '',
                "nitrogeno": '',
                "potasio": ''
            },
            "greenhouse": {
                "width": '',
                "height": '',
                "daytemperature": '',
                "nighttemperature": ''
      
            },
            "waterproperties": {
                "quantity": '',
                "ph": '',
                "conductivity": ''
            },
            "groundproperties": {
                "ph": ''
            }
        },
        "dataout": {
            "totalplants": '',
            "totalharvest": '',
            "tomatoaverage": '',
            "consumerwater": '',
            "userwater": '',
            "cnfosforo": '',
            "cnnitrogeno": '',
            "cnpotasio": ''
        }
      }
}

export default (state = initialState, action) => {
    switch (action.type) {
        case 'SET_DATAIN':
            return Object.assign({}, state, {
                dataIn: action.data
            });
        case 'CLEAR_DATAIN':
            return initialState;
    
        default:
            return state;
    }
}