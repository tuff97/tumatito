// Packages
import React from 'react';


/*<Input
    text={'100'}
    title={'Potasio'}
    unit={'K'}
    />*/

    // Componente presentacional de un Input con su label
export const Input = (props) => {
    return (
        <div className='input-data'>
            <div className='input-data__content'>
                {
                    props.tagInput
                        ?
                            <input
                                name={props.name}
                                value={props.value}
                                onChange={props.onChange}
                                id={props.id}
                                className='input-data__content__input'
                                type={props.type}
                                required
                                />
                        :
                            <div id={props.id} className='input-data__content__input'>
                                {props.text}
                            </div>
                }
                
                    <label htmlFor={props.id} className='input-data__content__label'>{props.title}</label>
                
                {
                    props.unit &&
                    <span className='input-data__medition'>{props.unit}</span>
                }
                {
                    props.required &&
                    <span className='input-data__required'>*</span>
                }
            </div>
        </div>
    );
}