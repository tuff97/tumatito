// Packages
import React, { Component } from 'react';
import { Line } from 'react-chartjs-2';

//firebase
import { onSimulation } from '../../firebase/database';
// styles
import './style.scss';

class Graphics extends Component {
    // Construimos la estructura de la grafica
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            chartData: {
                labels: ['Jitomates', 'promedio por planta', 'agua consumida', 'plantas'],
                datasets: [{
                    label: 'name 1',
                    fill: false,
                    data: [0,0,0,0],
                    backgroundColor:'#54D8FF',
                    borderColor: '#54D8FF',
                    borderWidth: 1.2
                },
                {
                    label: 'name 2',
                    fill: false,
                    data: [0,0,0,0],
                    backgroundColor:'#A3A0FB',
                    borderColor: '#A3A0FB',
                    borderWidth: 1.2
                },
                {
                    label: 'name 3',
                    fill: false,
                    data:[0,0,0,0],
                    backgroundColor:'#4CCBAB',
                    borderColor: '#4CCBAB',
                    borderWidth: 1.2
                }
                
            ],
                                  
            },
        }
    }
    // función para llenar la grafica con las ultimas 3 simulaciones
    fillData() {
        const data = this.state.data;
        if(data.length === 1) {
            this.setState({
                chartData: {
                    labels: ['Jitomates', 'promedio por planta', 'agua consumida', 'plantas'],
                    datasets: [{
                        label: data[0].name,
                        fill: false,
                        data:
                            [
                                data[0].dataout.totalharvest,
                                data[0].dataout.tomatoaverage,
                                data[0].dataout.consumerwater,
                                data[0].dataout.totalplants
                            ],
                        backgroundColor:'#54D8FF',
                        borderColor: '#54D8FF',
                        borderWidth: 1.2
                    }
                    
                ],
                                        
                }
            })

        }else if(data.length === 2) {
            this.setState({
                chartData: {
                    labels: ['Jitomates', 'promedio por planta', 'agua consumida', 'plantas'],
                    datasets: [{
                        label: data[0].name,
                        fill: false,
                        data:
                            [
                                data[0].dataout.totalharvest,
                                data[0].dataout.tomatoaverage,
                                data[0].dataout.consumerwater,
                                data[0].dataout.totalplants
                            ],
                        backgroundColor:'#54D8FF',
                        borderColor: '#54D8FF',
                        borderWidth: 1.2
                    },
                    {
                        label: data[1].name,                    
                        fill: false,
                        data:
                            [
                                data[1].dataout.totalharvest,
                                data[1].dataout.tomatoaverage,
                                data[1].dataout.consumerwater,
                                data[1].dataout.totalplants
                            ],
                        backgroundColor:'#A3A0FB',
                        borderColor: '#A3A0FB',
                        borderWidth: 1.2
                    }
                    
                ],
                                        
                }
            })
        } else {
            this.setState({
                chartData: {
                    labels: ['Jitomates', 'promedio por planta', 'agua consumida', 'plantas'],
                    datasets: [{
                        label: data[0].name,
                        fill: false,
                        data:
                            [
                                data[0].dataout.totalharvest,
                                data[0].dataout.tomatoaverage,
                                data[0].dataout.consumerwater,
                                data[0].dataout.totalplants
                            ],
                        backgroundColor:'#54D8FF',
                        borderColor: '#54D8FF',
                        borderWidth: 1.2
                    },
                    {
                        label: data[1].name,                    
                        fill: false,
                        data:
                            [
                                data[1].dataout.totalharvest,
                                data[1].dataout.tomatoaverage,
                                data[1].dataout.consumerwater,
                                data[1].dataout.totalplants
                            ],
                        backgroundColor:'#A3A0FB',
                        borderColor: '#A3A0FB',
                        borderWidth: 1.2
                    },
                    {
                        label: data[2].name,                    
                        fill: false,
                        data:
                            [
                                data[2].dataout.totalharvest,
                                data[2].dataout.tomatoaverage,
                                data[2].dataout.consumerwater,
                                data[2].dataout.totalplants
                            ],
                        backgroundColor:'#4CCBAB',
                        borderColor: '#4CCBAB',
                        borderWidth: 1.2
                    }
                    
                ],
                                        
                }
            })
        }
        

    }


    // Obtenemos la sultimas 3 simulaciones del usuario
    componentDidMount() {
        let array = [];
        onSimulation(3).then(data => {
            Object.keys(data).forEach(element => {
                const obj = {
                    datain: data[element].data.datain,
                    dataout: data[element].data.dataout,
                    name: data[element].name,
                    numberSimulation: data[element].numberSimulation
                }
                array.push(obj)
            });
            this.setState({
                data: array
            })
            this.fillData();
        })
    }
    
    render() {
        // Generamos la Gráfica
        return (
            <div className='graphic'>
                <Line
                    data={this.state.chartData}
                    respon
                    height={300}
                    options = {{
                        maintainAspectRatio: false,
                        responsive: true,
                    }}
                />
            </div>
        );
    }
}

export default Graphics;
