// Packages
import React, { Component } from 'react';
import ReactTable from 'react-table';

// Files
import 'react-table/react-table.css'
import './style.scss';

// firebase
import { removeSimulation } from '../../firebase/database'


class Table extends Component {
    // Instanciamos el constructor para los valores iniciales del correo, contraseña y el estado de     la notificación
    constructor(props) {
        super(props)
        this.state = {
            data: {},
        }
    }

    // Función que se ejecuta antes de montar el componente
    componentWillMount() {

        // Seteamos el estado "data" con las simulaciones que le pasamos por medio d eprops
        this.setState({
            data: this.props.data
        })
    }
    deleteSimulation(name) {
      const uid = JSON.parse(localStorage.getItem('user')).uid;
      removeSimulation(uid, name)
      this.props.onRefesh()
    }


    render() {

        // Creamos la estructura de las columnas
        const colums = [
            {
                Header: 'Invernadero',
                columns: [
                    {
                        Header: 'Nombre',
                        accessor:'name',
                        width: 150,
                        Cell: props => <span>{props.value}</span>,
                    },
                    {
                        Header: 'Ancho',
                        accessor:'datain.greenhouse.width',
                        Cell: props => <span>{props.value}</span>,
                    },
                    {
                        Header: 'Largo',
                        accessor:'datain.greenhouse.height',
                        Cell: props => <span>{props.value}</span>,
                    },
                ]
            },
            {
                Header: 'Temperatura',
                columns: [
                    {
                        Header: 'Día',
                        accessor:'datain.greenhouse.daytemperature',
                        Cell: props => <span>{props.value}</span>,
                    },
                    {
                        Header: 'Noche',
                        accessor:'datain.greenhouse.nighttemperature',
                        Cell: props => <span>{props.value}</span>,
                    }
                ]
            },
            {
                Header: 'Tierra',
                columns: [
                    {
                        Header: 'Ph',
                        accessor: 'datain.groundproperties.ph',
                        Cell: props => <span>{props.value}</span>,
                    }
                ]
            },
            {
                Header: 'Agua',
                columns: [
                    {
                        Header: 'Ph',
                        accessor: 'datain.waterproperties.ph',
                        Cell: props => <span>{props.value}</span>,
                    },
                    {
                        Header: 'Conductividad',
                        accessor: 'datain.waterproperties.conductivity',
                        Cell: props => <span>{props.value}</span>,
                    },
                    {
                        Header: 'Fósforo',
                        accessor: 'datain.waterproperties.quantity',
                        Cell: props => <span>{props.value}</span>,
                    }
                ]
            },
            {
                Header: 'Resultados',
                columns: [
                    {
                        Header: 'Plantas',
                        accessor: 'dataout.totalplants',
                        Cell: props => <span>{props.value}</span>,
                    },
                    {
                        Header: 'Cosecha',
                        accessor: 'dataout.totalharvest',
                        Cell: props => <span>{props.value}</span>,
                    },
                    {
                        Header: 'Promedio p.p',
                        accessor: 'dataout.tomatoaverage',
                        Cell: props => <span>{props.value}</span>,
                    },
                    {
                        Header: 'Agua C.',
                        accessor: 'dataout.consumerwater',
                        width: 150,
                        Cell: props => <span>{props.value}</span>,
                    },
                    {
                        Header: 'Agua Usuario',
                        accessor: 'dataout.userwater',
                        Cell: props => <span>{props.value}</span>,
                    },
                    {
                        Header: 'Fosforo',
                        accessor: 'dataout.cnfosforo',
                        Cell: props => <span>{props.value}</span>,
                    },
                    {
                        Header: 'Nitrogeno',
                        accessor: 'dataout.cnnitrogeno',
                        Cell: props => <span>{props.value}</span>,
                    },
                    {
                        Header: 'Potasio',
                        accessor: 'dataout.cnpotasio',
                        Cell: props => <span>{props.value}</span>,
                    },
                    {
                      Header: 'eliminar',
                      accessor:'name',
                    Cell: props => <button className='btn-remove' onClick={this.deleteSimulation.bind(this, props.value)} >eliminar</button>
                  },


                ]
            }

        ]




        // Obtenemos las simulaciones qu epasamos por medio de props
        const { data } = this.props;

        // Generamos la tabla de las simulaciones del usuario
        return (
            <div className='table'>
                <h1 className='table__history'>Historial</h1>
                <p className='table__msg'>Encontraras todas las simulaciones que realices en la siguiente table.</p>
                <ReactTable
                    data={data}
                    columns={colums}
                    defaultPageSize={10}
                    resizable={false}
                    filterable={true}
                    noDataText={'No se encontrados resultados'}
                    className="-striped -highlight"

                />
            </div>
        );
    }
}

export default Table;
