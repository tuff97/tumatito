// Packages
import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

// auth
import { logout } from '../../firebase/authentication';

//actions
import { setPlant, resetPlant } from '../../containers/Home/actions';

// Components
import { SVG, Arrow, SVGHome, SVGData, SVGDashboard, SVGHistory, NavProhibition } from '../SVG';

// styles.scss
import './style.scss';

class Navigation extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userEmail: '',
        }
    }
    componentWillMount() {
        this.setState({
            userEmail: JSON.parse(localStorage.getItem('user')).email,
        })
    }

    handleClick() {
        this.refs.nav.classList.toggle('navigation-mobile__list--active');
        this.refs.showHidden.classList.toggle('show')

    }
    onClick() {
        this.props.setPlant(1);
        this.props.resetPlant();
        console.log('plplplplp',this.props.history)
        logout().then(() => {
            this.props.history.push('/ingresar');
        })
    }

    stopPropagation(e) {
        e.stopPropagation();
    }
    render() {

        // Maquetación de la navegación
        console.log('plant number', this.props.plant)
        let animate = '';
        if(this.props.plant !== 9) {
            animate = 'progress__bar--animate';
        }
        return (
            <div className='navigation-mobile' onClick={this.handleClick.bind(this)}>
                <div className='navigation-mobile__header'>
                    <div>
                        <Link to='/'><SVG size={35}/></Link>
                    </div>

                    <div className='progress-bar__container'>
                        <p className='progress-bar__data'>semana<span>{this.props.plant}</span>de<span>9</span></p>
                        <div className='progress-bar__placeholder'>
                            <div className={`progress-bar ${animate}`}></div>
                        </div>
                    </div>
                    <div>
                        <p className='email-user email-user__nav'>{this.state.userEmail}</p>
                    </div>

                    <div  ref='showHidden' className='show-hidden'>
                        <Arrow onclick={this.handleClick.bind(this)}/>
                    </div>
                    
                </div>
                
                <ul className='navigation-mobile__list' ref='nav' onClick={this.handleClick.bind(this)}>
                    <div>
                            <p className='email-user email-user__list'>{this.state.userEmail}</p>
                    </div>
                    <li className='navigation-mobile__item'>
                        <NavLink
                            onClick={this.handleClick.bind(this)}
                            exact
                            to='/'
                            className='navigation__link'
                            activeClassName='navigation__link--active'>
                                <span className='navigation__link-svg'><SVGHome /></span>
                                Simulación
                            </NavLink>
                    </li>
                    <li className='navigation-mobile__item'>
                        <NavLink
                            onClick={this.handleClick.bind(this)}
                            exact
                            to='/datos'
                            className='navigation__link'
                            activeClassName='navigation__link--active'>
                                <span className='navigation__link-svg'><SVGData /></span>
                                Datos
                            </NavLink>
                    </li>
                    <li className='navigation-mobile__item'>
                        <NavLink
                            onClick={this.handleClick.bind(this)}
                            exact
                            to='/estadisticas'
                            className='navigation__link'
                            activeClassName='navigation__link--active'>
                                <span className='navigation__link-svg'><SVGDashboard /></span>
                                Estadisticas
                            </NavLink>
                    </li>
                    <li className='navigation-mobile__item'>
                        <NavLink
                            onClick={this.handleClick.bind(this)}
                            exact
                            to='/historial'
                            className='navigation__link'
                            activeClassName='navigation__link--active'>
                                <span className='navigation__link-svg'><SVGHistory /></span>
                                Historial
                            </NavLink>
                    </li>
                    <li className='navigation-mobile__item'>
                        <button className='btn' onClick={this.onClick.bind(this)}>Cerrar sesión</button>                        
                    </li>
                </ul>
                {
                    animate !== '' &&
                    <div className='top__loading' onClick={this.stopPropagation.bind(this)}>
                        <NavProhibition />
                    </div>
                }
            </div>
        );
    }
}
const mapStateToPtops = ({ home }) => {
    return {
        plant: home.plant,
    }
}

export default connect(mapStateToPtops, { setPlant, resetPlant }) (Navigation);
