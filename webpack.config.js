const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const devMode = process.env.NODE_ENV !== 'production';
console.log('devMode client', devMode)

// webpack.config.js
const js = {
    test: /\.(js|jsx)$/,
    exclude: /node_modules/,
    use: {
        loader: 'babel-loader',
    }
}
const css = {
    test: /\.(sa|sc|c)ss$/,
    use: [
        devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
        'css-loader',
        'sass-loader',
    ],    
}

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.join(__dirname, './dist'),
        filename: 'bundle.js',
        publicPath: '/',
    },
    devServer: {
        historyApiFallback: true,
        host: "0.0.0.0",
        port: 3000,
        hot: true,
    },

    module: {
        rules: [js, css]
    },

    resolve: {
        extensions: ['.js', '.jsx']
    },

   plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.html",
            //ruta explicita por la salida que es en ./dist
            filename: "index.html"
        }),
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            path: path.join(__dirname, "/dist"),
            filename: 'main.css',
            //chunkFilename: '[id].css',
        }),
    ]
};