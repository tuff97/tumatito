**Versiones de node, npm y yarn**

- Node: 10.15.3

- npm: 6.4.1

- yarn: 1.16.0

**Install dependencias**

- `yarn install`

**Para correr en desarrollo**

- `yarn start`

**generar el build:**

- `yarn run build`
